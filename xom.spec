%define with_dom4j %{?_with_dom4j:1}%{!?_with_dom4j:0}
%define without_dom4j %{!?_with_dom4j:1}%{?_with_dom4j:0}
Summary:             XML Object Model
Name:                xom
Version:             1.3.9
Release:             1
Epoch:               0
License:             LGPLv2
URL:                 http://www.xom.nu
Source0:             https://github.com/elharo/xom/archive/v%{version}/%{name}-%{version}.tar.gz
BuildRequires:       ant >= 0:1.6 javapackages-local javapackages-tools jarjar jaxen junit
BuildRequires:       xerces-j2
%if %{with_dom4j}
BuildRequires:       dom4j
%endif
BuildRequires:       xml-commons-apis tagsoup java-devel xml-commons-resolver servlet
Requires:            jaxen xerces-j2 xml-commons-apis
BuildArch:           noarch
%description
XOM is a new XML object model. It is an open source (LGPL),
tree-based API for processing XML with Java that strives
for correctness, simplicity, and performance, in that order.
XOM is designed to be easy to learn and easy to use. It
works very straight-forwardly, and has a very shallow
learning curve. Assuming you're already familiar with XML,
you should be able to get up and running with XOM very quickly.

%package javadoc
Summary:             API documentation for %{name}
%description javadoc
This package provides %{summary}.

%package demo
Summary:             Samples for %{name}
Requires:            %{name} = %{version}-%{release}
%description demo
This package provides %{summary}.

%prep
%autosetup -n %{name}-%{version} -p1
find \( -name '*.jar' -or -name '*.class' \) -delete
for s in src/nu/xom/tests/BuilderTest.java\
 src/nu/xom/tests/SerializerTest.java;do
  native2ascii -encoding UTF8 ${s} ${s}
done
sed -i 's/\r//g' LICENSE.txt
sed -i "s,59 Temple Place,51 Franklin Street,;s,Suite 330,Fifth Floor,;s,02111-1307,02110-1301,"  $(find -name "*.java") \
 LICENSE.txt lgpl.txt
rm -rf ./src/nu/xom/tools/XHTMLJavaDoc.java

%build
mkdir -p lib
pushd lib
ln -sf $(build-classpath junit) junit.jar
ln -sf $(build-classpath xerces-j2) xercesImpl-2.12.2.jar
ln -sf $(build-classpath xml-commons-apis) xmlParserAPIs.jar
ln -sf $(build-classpath jaxen) jaxen.jar
while IFS=':' read -ra JARJAR_JARS; do
    for j in "${JARJAR_JARS[@]}";do
        ln -sf $j $(basename $j .jar)-1.0.jar
    done
done <<<$(build-classpath jarjar)
popd
mkdir -p lib2
pushd lib2
ln -sf $(build-classpath tagsoup) tagsoup-1.2.jar
ln -sf $(build-classpath xml-commons-resolver) resolver.jar
%if %{with_dom4j}
ln -sf $(build-classpath dom4j) dom4j.jar
%endif
ln -sf $(build-classpath servlet) servlet.jar
popd
ant -v jar samples betterdoc maven2
pushd build/apidocs
for f in `find -name \*.css -o -name \*.html`; do
  sed -i 's/\r//g' $f
done
popd
mv build/maven2/project.xml build/maven2/pom.xml
%pom_add_dep jaxen:jaxen build/maven2/pom.xml
%mvn_artifact build/maven2/pom.xml build/%{name}-%{version}.jar
%mvn_alias xom:xom com.io7m.xom:xom

%install
%mvn_install
ln -s xom/%{name}.jar %{buildroot}%{_javadir}/%{name}.jar
install -d -m 755 %{buildroot}%{_javadocdir}/%{name}
cp -pr build/apidocs/* %{buildroot}%{_javadocdir}/%{name}
install -d -m 755 %{buildroot}%{_datadir}/%{name}
install -m 644 build/xom-samples.jar %{buildroot}%{_datadir}/%{name}
install -d -m 755 %{buildroot}%{_mavenpomdir}
ln -s xom/xom.pom %{buildroot}%{_mavenpomdir}/JPP-%{name}.pom

%files -f .mfiles
%doc README.txt
%doc LICENSE.txt
%doc Todo.txt
%doc lgpl.txt
%{_javadir}/%{name}.jar
%{_mavenpomdir}/JPP-%{name}.pom

%files javadoc
%{_javadocdir}/%{name}
%doc LICENSE.txt
%doc lgpl.txt

%files demo
%dir %{_datadir}/%{name}
%{_datadir}/%{name}/xom-samples.jar

%changelog
* Thu Jan 09 2025 yaoxin <1024769339@qq.com> - 0:1.3.9-1
- Update to 1.3.9:
  * Purely internal changes that improve compatibility with Java 17+.
  * Xerces dependency has been updated to the latest release and Xalan has been removed completely.

* Wed Sep 20 2023 Ge Wang <wang__ge@126.com> - 0:1.3.7-1
- Update to version 1.3.7

* Thu Jul 23 2020 Jeffery.Gao <gaojianxing@huawei.com> - 1.2.10-1
- Package init
